﻿namespace Gomez.Core.DocumentDb
{
    public interface ITransaction<out TContext> : ITransaction
    {
        TContext Context { get; }
    }

    public interface ITransaction
    {
        ITransaction Create<T>(T item, ITransactionOptions options) where T : IDocumentEntity;
        ITransaction DeleteItem(string id, ITransactionOptions options);
        ITransaction ReadItem(string id, ITransactionOptions options);
        ITransaction ReplaceItem<T>(T item, ITransactionOptions options) where T : IDocumentEntity;
        ITransaction UpsertItem<T>(T item, ITransactionOptions options) where T : IDocumentEntity;
    }
}