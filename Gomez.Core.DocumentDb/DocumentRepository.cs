﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Gomez.Core.DocumentDb
{
    public abstract class DocumentRepository<T> : IDocumentRepository<T> where T : IDocumentEntity
    {
        protected readonly string _collectionName;
        protected readonly IDocumentDbContext _context;
        protected readonly Func<T, object> _partitionKeyFunc;

        protected DocumentRepository(IDocumentDbContext context, string collectionName)
        {
            _context = context;
            _collectionName = collectionName;
        }

        protected DocumentRepository(IDocumentDbContext context, string collectionName, Expression<Func<T, object>> partitionKeyExp)
        {
            _context = context;
            _collectionName = collectionName;
            _partitionKeyFunc = partitionKeyExp.Compile();
        }

        public ItemOptions CreateOptions(T entity, ItemOptions options = null)
        {
            options ??= new ItemOptions();
            if (_partitionKeyFunc == null)
            {
                return options;
            }

            var pk = _partitionKeyFunc(entity);
            options.PartitionKey = pk;
            return options;
        }

        private object GetPartitionKey(T entity)
        {
            if (_partitionKeyFunc == null)
            {
                return null;
            }

            return _partitionKeyFunc(entity);
        }

        public Task<T> CreateAsync(IItemOptions options, T entity)
        {
            return _context.CreateAsync(_collectionName, entity, options);
        }

        public Task<T> CreateAsync(T entity)
        {
            var partitionKey = GetPartitionKey(entity);
            return CreateAsync(new ItemOptions() { PartitionKey = partitionKey }, entity);
        }

        public Task<T> ReplaceAsync(IItemOptions options, T entity)
        {
            return _context.ReplaceAsync(_collectionName, entity, options);
        }

        public Task<T> ReplaceAsync(T entity)
        {
            var partitionKey = GetPartitionKey(entity);
            return ReplaceAsync(new ItemOptions() { PartitionKey = partitionKey }, entity);
        }

        public Task DeleteAsync(IItemOptions options, string id, string eTag = "")
        {
            return _context.DeleteAsync<T>(_collectionName, id, options, eTag);
        }

        public Task DeleteAsync(string id, object partitionKey, string eTag = "")
        {
            return DeleteAsync(
                new ItemOptions() { PartitionKey = partitionKey, EnableContentResponseOnWrite = false },
                id,
                eTag);
        }

        public Task DeleteAsync(T entity)
        {
            var partitionKey = GetPartitionKey(entity);
            return DeleteAsync(
                new ItemOptions() { PartitionKey = partitionKey, EnableContentResponseOnWrite = false },
                entity.Id,
                entity.ETag);
        }

        public Task DeleteAsync(IItemOptions options, T entity)
        {
            return DeleteAsync(
                options,
                entity.Id,
                entity.ETag);
        }

        public virtual IQueryable<T> GetQuery()
        {
            return _context.GetQuery<T>(_collectionName);
        }

        public virtual IQueryable<T> GetQuery(IQueryOptions options)
        {
            return _context.GetQuery<T>(_collectionName, options);
        }

        public virtual Task<IQueryable<T>> GetQueryAsync(IQueryOptions options)
        {
            return _context.GetQueryAsync<T>(_collectionName, options);
        }

        public Task<T> ReadFromQueryAsync(IQueryOptions queryOptions, IItemOptions itemOptions, Expression<Func<T, bool>> predicate)
        {
            return _context.ReadFromQueryAsync<T>(_collectionName, predicate, queryOptions, itemOptions, _partitionKeyFunc);
        }

        public Task<T> ReadFromQueryAsync(Expression<Func<T, bool>> predicate)
        {
            return _context.ReadFromQueryAsync<T>(
                _collectionName,
                predicate,
                new QueryOptions() { MaxItemCount = 1 },
                new ItemOptions(), _partitionKeyFunc);
        }

        public Task<T> ReadFromQueryAsync(IQueryable<T> query, IQueryOptions queryOptions, IItemOptions itemOptions)
        {
            return _context.ReadFromQueryAsync<T>(_collectionName, query, queryOptions, itemOptions, _partitionKeyFunc);
        }

        public Task<(IEnumerable<T> resource, string continuationToken)> ExecuteSqlAsync(
            IQueryOptions options,
            string sqlText,
            Dictionary<string, object> parameters = null)
        {
            return _context.ExecuteSqlAsync<T>(_collectionName, sqlText, options, parameters);
        }

        public Task<(IEnumerable<T> resource, string continuationToken)> ExecuteSqlAsync(
            string sqlText,
            object partitionKey,
            Dictionary<string, object> parameters = null)
        {
            return _context.ExecuteSqlAsync<T>(_collectionName, sqlText, new QueryOptions() { PartitionKey = partitionKey }, parameters);
        }

        public Task<T> ExecuteStoredProcedure(
            IStoredProcOptions options,
            string sprocId,
            params object[] parameters)
        {
            return _context.ExecuteStoredProcedureAsync<T>(_collectionName, sprocId, options, parameters);
        }

        public Task<T> ExecuteStoredProcedure(
            string sprocId,
            object partitionKey,
            params object[] parameters)
        {
            return _context.ExecuteStoredProcedureAsync<T>(_collectionName, sprocId, new StoredProcOptions() { PartitionKey = partitionKey }, parameters);
        }

        public Task<T> ReadAsync(IItemOptions options, string id)
        {
            return _context.ReadAsync<T>(_collectionName, id, options);
        }

        public Task<T> ReadAsync(string id, object partitionKey)
        {
            return ReadAsync(new ItemOptions() { PartitionKey = partitionKey }, id);
        }

        public Task<T> UpdateAsync(IItemOptions options, T entity, Func<T, T> updateAction)
        {
            return _context.UpdateAsync(_collectionName, entity, updateAction, options);
        }

        public Task<T> UpdateAsync(T entity, Func<T, T> updateAction)
        {
            var partitionKey = GetPartitionKey(entity);
            return UpdateAsync(new ItemOptions() { PartitionKey = partitionKey }, entity, updateAction);
        }

        public Task<T> UpsertAsync(IItemOptions options, T entity)
        {
            return _context.UpsertAsync(_collectionName, entity, options);
        }

        public Task<T> UpsertAsync(T entity)
        {
            var partitionKey = GetPartitionKey(entity);
            return UpsertAsync(new ItemOptions() { PartitionKey = partitionKey }, entity);
        }

        public Task<T> UpsertAsync(IItemOptions options, T entity, Func<T, T> updateAction)
        {
            return _context.UpsertAsync(_collectionName, entity, updateAction, options);
        }

        public Task<T> UpsertAsync(T entity, Func<T, T> updateAction)
        {
            var partitionKey = GetPartitionKey(entity);
            return UpsertAsync(new ItemOptions() { PartitionKey = partitionKey }, entity, updateAction);
        }

        public Task<List<BulkResponse<T>>> BulkInsertAsync(IEnumerable<T> entities, IItemOptions options)
        {
            return _context.BulkInsertAsync(_collectionName, entities, options, _partitionKeyFunc);
        }

        public Task<List<BulkResponse<T>>> BulkInsertAsync(IEnumerable<T> entities)
        {
            return _context.BulkInsertAsync(_collectionName, entities, new ItemOptions(), _partitionKeyFunc);
        }

        public Task<List<BulkResponse<T>>> BulkUpsertAsync(IEnumerable<T> entities, IItemOptions options)
        {
            return _context.BulkUpsertAsync(_collectionName, entities, options, _partitionKeyFunc);
        }

        public Task<List<BulkResponse<T>>> BulkUpsertAsync(IEnumerable<T> entities)
        {
            return _context.BulkUpsertAsync(_collectionName, entities, new ItemOptions(), _partitionKeyFunc);
        }

        public Task<List<BulkResponse<T>>> BulkReplaceAsync(IEnumerable<T> entities, IItemOptions options)
        {
            return _context.BulkReplaceAsync(_collectionName, entities, options, _partitionKeyFunc);
        }

        public Task<List<BulkResponse<T>>> BulkReplaceAsync(IEnumerable<T> entities)
        {
            return _context.BulkReplaceAsync(_collectionName, entities, new ItemOptions(), _partitionKeyFunc);
        }

        public Task<List<BulkResponse<T>>> BulkDeleteAsync(IEnumerable<T> entities, IItemOptions options)
        {
            return _context.BulkDeleteAsync(_collectionName, entities, options, _partitionKeyFunc);
        }

        public Task<List<BulkResponse<T>>> BulkDeleteAsync(IEnumerable<T> entities)
        {
            return _context.BulkDeleteAsync(_collectionName, entities, new ItemOptions(), _partitionKeyFunc);
        }
    }
}
