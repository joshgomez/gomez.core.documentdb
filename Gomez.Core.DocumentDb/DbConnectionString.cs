﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Common;

namespace Gomez.Core.DocumentDb
{
    public class DbConnectionString
    {
        public static List<string> AuthKeyNames { get; set; } = new List<string>()
        {
            "AccountKey"
        };

        public static List<string> ServiceEndpointNames { get; set; } = new List<string>()
        {
            "AccountEndpoint"
        };

        public static List<string> DatabaseNames { get; set; } = new List<string>()
        {
            "Database"
        };

        public DbConnectionString(string connectionString)
        {
            InitProperties(connectionString);
        }

        public DbConnectionString(string connectionStringName, IConfiguration configuration)
        {
            InitProperties(configuration.GetConnectionString(connectionStringName));
        }

        private void InitProperties(string connectionString)
        {
            // Use this generic builder to parse the connection string
            DbConnectionStringBuilder builder = new DbConnectionStringBuilder
            {
                ConnectionString = connectionString
            };

            foreach (var name in AuthKeyNames)
            {
                if (builder.TryGetValue(name, out object key))
                {
                    AuthKey = key.ToString();
                    break;
                }
            }

            foreach (var name in ServiceEndpointNames)
            {
                if (builder.TryGetValue(name, out object uri))
                {
                    ServiceEndpoint = new Uri(uri.ToString());
                    break;
                }
            }

            foreach (var name in DatabaseNames)
            {
                if (builder.TryGetValue(name, out object db))
                {
                    Database = db.ToString();
                    break;
                }
            }
        }

        public Uri ServiceEndpoint { get; set; }

        public string AuthKey { get; set; }

        public string Database { get; set; }
    }
}
