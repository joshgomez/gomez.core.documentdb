﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Gomez.Core.DocumentDb
{
    public interface IDocumentDbContext : IDisposable
    {
        public IDistributedCache Cache { get; }
        IQueryable<T> GetQuery<T>(string collectionName);
        IQueryable<T> GetQuery<T>(string collectionName, IQueryOptions options);
        Task<T> ReadAsync<T>(string collectionName, string id, IItemOptions options)
            where T : IDocumentEntity;
        Task<T> UpdateAsync<T>(string collectionName, T entity, Func<T, T> updateAction, IItemOptions options)
            where T : IDocumentEntity;
        Task<T> CreateAsync<T>(string collectionName, T entity, IItemOptions options)
            where T : IDocumentEntity;
        Task<T> ReplaceAsync<T>(string collectionName, T entity, IItemOptions options)
            where T : IDocumentEntity;
        /// <summary>
        /// Concurrency safe upsert, if the document exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collectionName"></param>
        /// <param name="entity"></param>
        /// <param name="updateAction"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        Task<T> UpsertAsync<T>(string collectionName, T entity, Func<T, T> updateAction, IItemOptions options)
            where T : IDocumentEntity;
        Task<T> UpsertAsync<T>(string collectionName, T entity, IItemOptions options)
            where T : IDocumentEntity;
        Task DeleteAsync<T>(string collectionName, string id, IItemOptions options, string eTag = "") where T : IDocumentEntity;
        Task<IContinuedList<T>> ToContinuedListAsync<T>(IQueryable<T> query);
        Task<List<T>> ToListAsync<T>(IQueryable<T> query);
        Task<Dictionary<TKey, TValue>> ToDictionaryAsync<TEntity, TKey, TValue>(IQueryable<TEntity> query, Func<TEntity, TKey> keySelector, Func<TEntity, TValue> valueSelector);
        IAsyncEnumerable<T> ToAsyncEnumerable<T>(IQueryable<T> query);
        Task<T> FirstOrDefaultAsync<T>(IQueryable<T> query, System.Linq.Expressions.Expression<Func<T, bool>> predicate);
        Task<T> FirstOrDefaultAsync<T>(IQueryable<T> query);
        Task<List<BulkResponse<T>>> BulkInsertAsync<T>(string collectionName, IEnumerable<T> entities, IItemOptions options, Func<T, object> getPkFunc) where T : IDocumentEntity;
        Task<(IEnumerable<T> resource, string continuationToken)> ExecuteSqlAsync<T>(string collectionName, string sqlText, IQueryOptions options, Dictionary<string, object> parameters = null);
        Task<T> ExecuteStoredProcedureAsync<T>(string collectionName, string sprocId, IStoredProcOptions options, params object[] parameters);
        Task<T> ReadFromQueryAsync<T>(string collectionName, IQueryable<T> query, IQueryOptions queryOptions, IItemOptions itemOptions, Func<T, object> partitionKeyFunc) where T : IDocumentEntity;
        Task<T> ReadFromQueryAsync<T>(string collectionName, Expression<Func<T, bool>> predicate, IQueryOptions queryOptions, IItemOptions itemOptions, Func<T, object> partitionKeyFunc) where T : IDocumentEntity;
        ITransaction CreateTransaction(string collectionName, object partitionKey);
        IAsyncEnumerable<TransactionResponse> ExecuteTransactionAsync(ITransaction transaction);
        Task<List<BulkResponse<T>>> BulkUpsertAsync<T>(string collectionName, IEnumerable<T> entities, IItemOptions options, Func<T, object> getPkFunc) where T : IDocumentEntity;
        Task<List<BulkResponse<T>>> BulkReplaceAsync<T>(string collectionName, IEnumerable<T> entities, IItemOptions options, Func<T, object> getPkFunc) where T : IDocumentEntity;
        Task<List<BulkResponse<T>>> BulkDeleteAsync<T>(string collectionName, IEnumerable<T> entities, IItemOptions options, Func<T, object> getPkFunc) where T : IDocumentEntity;
        Task<int> CountAsync<T>(IQueryable<T> query);
        Task<T> MaxAsync<T>(IQueryable<T> query);
        Task<T> MinAsync<T>(IQueryable<T> query);
        Task<IQueryable<T>> GetQueryAsync<T>(string collectionName, IQueryOptions options);
    }
}
