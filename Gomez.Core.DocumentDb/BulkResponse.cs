﻿using System.Net;

namespace Gomez.Core.DocumentDb
{
    public class BulkResponse<T>
    {
        public T Document { get; set; }
        public string Message { get; set; }
        public HttpStatusCode Status { get; set; } = HttpStatusCode.OK;
    }
}
