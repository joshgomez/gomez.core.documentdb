﻿using System.IO;
using System.Net;

namespace Gomez.Core.DocumentDb
{
    public class TransactionResponse
    {
        public Stream Document { get; set; }
        public string Message { get; set; }
        public HttpStatusCode Status { get; set; } = HttpStatusCode.OK;
    }
}
