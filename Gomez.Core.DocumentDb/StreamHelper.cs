﻿using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace Gomez.Core.DocumentDb
{
    public static class StreamHelper
    {
        public static async Task<T> FromStream<T>(Stream stream)
        {
            using var streamReader = new StreamReader(stream);
            return JsonConvert.DeserializeObject<T>(await streamReader.ReadToEndAsync());
        }
    }
}
