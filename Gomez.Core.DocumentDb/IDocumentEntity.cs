﻿using Newtonsoft.Json;

namespace Gomez.Core.DocumentDb
{
    public interface IDocumentEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("_etag")]
        public string ETag { get; set; }
    }
}
