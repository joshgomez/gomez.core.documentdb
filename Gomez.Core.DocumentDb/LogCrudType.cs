﻿namespace Gomez.Core.DocumentDb
{
    public enum LogCrudType
    {
        Read,
        Create,
        Update,
        Replace,
        Upsert,
        Delete,
        BulkInsert,
        BulkUpsert,
        BulkReplace,
        BulkDelete
    }
}
