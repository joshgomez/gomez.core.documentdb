@echo off
cd %~dp0
nuget.exe pack Gomez.Core.DocumentDb.csproj -IncludeReferencedProjects -Properties Configuration=Release -OutputDirectory "D:\NuGetPackages"