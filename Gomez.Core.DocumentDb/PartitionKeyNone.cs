﻿namespace Gomez.Core.DocumentDb
{
    /// <summary>
    /// Can be used to pass as a object if document 
    /// provider support collection without parition keys
    /// </summary>
    public struct PartitionKeyNone
    {
        public static PartitionKeyNone Default => new PartitionKeyNone();

        public override bool Equals(object obj)
        {
            return GetType() == obj.GetType();
        }

        public override int GetHashCode()
        {
            return 1;
        }

        public static bool operator ==(PartitionKeyNone left, PartitionKeyNone right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(PartitionKeyNone left, PartitionKeyNone right)
        {
            return !(left == right);
        }
    }
}
