﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Gomez.Core.DocumentDb
{
    public interface IDocumentRepository<T> where T : IDocumentEntity
    {
        IQueryable<T> GetQuery();
        IQueryable<T> GetQuery(IQueryOptions options);
        Task<T> ReadAsync(IItemOptions options, string id);
        Task<T> ReadAsync(string id, object partitionKey);

        Task<T> UpdateAsync(IItemOptions options, T entity, Func<T, T> updateAction);
        Task<T> UpdateAsync(T entity, Func<T, T> updateAction);

        Task<T> UpsertAsync(IItemOptions options, T entity, Func<T, T> updateAction);
        Task<T> UpsertAsync(T entity, Func<T, T> updateAction);
        Task<T> UpsertAsync(IItemOptions options, T entity);
        Task<T> UpsertAsync(T entity);

        Task<T> CreateAsync(IItemOptions options, T entity);
        Task<T> CreateAsync(T entity);

        Task<T> ReplaceAsync(IItemOptions options, T entity);
        Task<T> ReplaceAsync(T entity);

        Task DeleteAsync(IItemOptions options, string id, string eTag = "");
        Task DeleteAsync(string id, object partitionKey, string eTag = "");
        Task DeleteAsync(T entity);
        Task DeleteAsync(IItemOptions options, T entity);
        Task<T> ReadFromQueryAsync(IQueryOptions queryOptions, IItemOptions itemOptions, Expression<Func<T, bool>> predicate);
        Task<T> ReadFromQueryAsync(Expression<Func<T, bool>> predicate);
        Task<T> ReadFromQueryAsync(IQueryable<T> query, IQueryOptions queryOptions, IItemOptions itemOptions);
        Task<T> ExecuteStoredProcedure(IStoredProcOptions options, string sprocId, params object[] parameters);
        Task<T> ExecuteStoredProcedure(string sprocId, object partitionKey, params object[] parameters);
        Task<(IEnumerable<T> resource, string continuationToken)> ExecuteSqlAsync(IQueryOptions options, string sqlText, Dictionary<string, object> parameters = null);
        Task<(IEnumerable<T> resource, string continuationToken)> ExecuteSqlAsync(string sqlText, object partitionKey, Dictionary<string, object> parameters = null);
        Task<List<BulkResponse<T>>> BulkInsertAsync(IEnumerable<T> entities, IItemOptions options);
        Task<List<BulkResponse<T>>> BulkInsertAsync(IEnumerable<T> entities);
        Task<List<BulkResponse<T>>> BulkUpsertAsync(IEnumerable<T> entities, IItemOptions options);
        Task<List<BulkResponse<T>>> BulkUpsertAsync(IEnumerable<T> entities);
        Task<List<BulkResponse<T>>> BulkReplaceAsync(IEnumerable<T> entities, IItemOptions options);
        Task<List<BulkResponse<T>>> BulkReplaceAsync(IEnumerable<T> entities);
        Task<List<BulkResponse<T>>> BulkDeleteAsync(IEnumerable<T> entities, IItemOptions options);
        Task<List<BulkResponse<T>>> BulkDeleteAsync(IEnumerable<T> entities);
        ItemOptions CreateOptions(T entity, ItemOptions options = null);
        Task<IQueryable<T>> GetQueryAsync(IQueryOptions options);
    }
}
