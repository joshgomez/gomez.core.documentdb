﻿using System.Collections.Generic;

namespace Gomez.Core.DocumentDb
{
    public class ContinuedList<T> : IContinuedList<T>
    {
        public ContinuedList()
        {

        }

        public ContinuedList(IContinuedList<T> list)
        {
            Documents = list.Documents;
            ContinuesToken = list.ContinuesToken;
        }

        public List<T> Documents { get; set; }
        public string ContinuesToken { get; set; }
    }
}
