﻿namespace Gomez.Core.DocumentDb
{
    public class TransactionOptions : ITransactionOptions
    {
        public bool? EnableContentResponseOnWrite { get; set; }
        public string IfNoneMatchEtag { get; set; }
        public string IfMatchEtag { get; set; }
    }
}
