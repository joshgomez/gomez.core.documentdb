﻿namespace Gomez.Core.DocumentDb
{
    public interface ITransactionOptions
    {
        bool? EnableContentResponseOnWrite { get; set; }
        string IfMatchEtag { get; set; }
        string IfNoneMatchEtag { get; set; }
    }
}