﻿namespace Gomez.Core.DocumentDb
{
    public class StoredProcOptions : IStoredProcOptions
    {
        public string SessionToken { get; set; }
        public object PartitionKey { get; set; }
        public string IfNoneMatchEtag { get; set; }
        public string IfMatchEtag { get; set; }
        public bool EnableScriptLogging { get; set; }
    }
}
