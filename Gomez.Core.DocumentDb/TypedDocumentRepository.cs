﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Gomez.Core.DocumentDb
{
    public abstract class TypedDocumentRepository<T> : DocumentRepository<T> where T : IDocumentEntity, ITypedDocumentEntity
    {
        private readonly string _type;

        protected TypedDocumentRepository(IDocumentDbContext context, string collectionName, string type)
            : base(context, collectionName)
        {
            _type = type;
        }

        protected TypedDocumentRepository(IDocumentDbContext context, string collectionName, Expression<Func<T, object>> partitionKeyExp, string type)
            : base(context, collectionName, partitionKeyExp)
        {
            _type = type;
        }

        public override IQueryable<T> GetQuery()
        {
            return base.GetQuery().Where(x => x.Type == _type);
        }

        public override IQueryable<T> GetQuery(IQueryOptions options)
        {
            return base.GetQuery(options).Where(x => x.Type == _type);
        }

        public override async Task<IQueryable<T>> GetQueryAsync(IQueryOptions options)
        {
            return (await base.GetQueryAsync(options)).Where(x => x.Type == _type);
        }
    }
}
