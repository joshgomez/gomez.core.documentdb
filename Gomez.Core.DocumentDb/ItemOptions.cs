﻿using System;
using System.Collections.Generic;

namespace Gomez.Core.DocumentDb
{
    public class ItemOptions : IItemOptions
    {
        public IEnumerable<string> PreTriggers { get; set; }
        public IEnumerable<string> PostTriggers { get; set; }
        public string SessionToken { get; set; }
        public bool? EnableContentResponseOnWrite { get; set; }
        public object PartitionKey { get; set; }
        public string IfNoneMatchEtag { get; set; }
        public string IfMatchEtag { get; set; }
        public DateTimeOffset? AbsoluteExpiration { get; set; }
        public TimeSpan? AbsoluteExpirationRelativeToNow { get; set; }
        public TimeSpan? SlidingExpiration { get; set; }
        public CacheOption Cache { get; set; } = CacheOption.Both;
    }
}
