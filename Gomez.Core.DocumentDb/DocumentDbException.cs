﻿using System;
using System.Runtime.Serialization;

namespace Gomez.Core.DocumentDb
{
    [Serializable]
    public class DocumentDbException : Exception
    {
        public DocumentDbException()
        {
        }

        public DocumentDbException(string message) : base(message)
        {
        }

        public DocumentDbException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DocumentDbException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}