﻿namespace Gomez.Core.DocumentDb
{
    public class QueryOptions : IQueryOptions
    {
        public int? ResponseContinuationTokenLimitInKb { get; set; }
        public bool? EnableScanInQuery { get; set; }
        public int? MaxItemCount { get; set; }
        public object PartitionKey { get; set; }
        public string SessionToken { get; set; }
        public string IfNoneMatchEtag { get; set; }
        public string IfMatchEtag { get; set; }
        public string RequestContinuation { get; set; }
    }
}
