﻿using System.Collections.Generic;

namespace Gomez.Core.DocumentDb
{
    public interface IContinuedList<T>
    {
        List<T> Documents { get; set; }
        string ContinuesToken { get; set; }
    }
}
