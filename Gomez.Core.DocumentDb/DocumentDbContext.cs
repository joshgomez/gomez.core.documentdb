﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Gomez.Core.DocumentDb
{
    public abstract class DocumentDbContext<T>
    {
        protected readonly string _endpointUrl;
        protected readonly string _databaseName;
        protected readonly ILogger<T> _logger;

        protected DocumentDbContext()
        {

        }

        protected DocumentDbContext(ILogger<T> logger) : this()
        {
            _logger = logger;
        }

        protected DocumentDbContext(string databaseName, ILogger<T> logger) : this(logger)
        {
            _databaseName = databaseName;
        }

        protected DocumentDbContext(string endpointUrl, string databaseName, ILogger<T> logger) : this(databaseName, logger)
        {
            _endpointUrl = endpointUrl;
        }

        protected static void CheckTries(int tries, int maxTries)
        {
            if (tries > maxTries)
            {
                throw new DocumentDbException($"Concurrency error: max tries reached. ({maxTries})");
            }
        }

        protected void SetCustomLogProps(ref Dictionary<string, object> logProps, (string key, object value)[] properties)
        {
            if (properties?.Length > 0)
            {
                foreach (var (key, value) in properties)
                {
                    logProps[key] = value;
                }
            }
        }

        protected void LogByRequestCharge(string message, double chargeCost)
        {
            if (chargeCost > 1000)
            {
                _logger.LogCritical(message);
                return;
            }

            if (chargeCost > 100)
            {
                _logger.LogError(message);
                return;
            }

            if (chargeCost > 30)
            {
                _logger.LogWarning(message);
                return;
            }

            if (chargeCost > 15)
            {
                _logger.LogInformation(message);
                return;
            }

            if (chargeCost > 10)
            {
                _logger.LogDebug(message);
                return;
            }

            _logger.LogTrace(message);
        }
    }
}
