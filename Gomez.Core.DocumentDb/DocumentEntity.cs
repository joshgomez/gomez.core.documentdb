﻿using Newtonsoft.Json;
using System;

namespace Gomez.Core.DocumentDb
{
    public class DocumentEntity : IDocumentEntity
    {
        public DocumentEntity()
        {
            Id = Guid.NewGuid().ToString();
        }

        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("_etag")]
        public string ETag { get; set; }
    }
}
