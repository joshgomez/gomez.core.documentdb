﻿using System;

namespace Gomez.Core.DocumentDb
{
    [Flags]
    public enum LogType
    {
        None = 0,
        Error = 1,
        Information = 2,
        Diagnostics = 4,
        All = Error | Information | Diagnostics
    }
}
