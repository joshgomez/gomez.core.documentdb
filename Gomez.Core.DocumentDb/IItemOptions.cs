﻿using System;
using System.Collections.Generic;

namespace Gomez.Core.DocumentDb
{
    public interface IItemOptions : IRequestOptions
    {
        bool? EnableContentResponseOnWrite { get; set; }
        IEnumerable<string> PostTriggers { get; set; }
        IEnumerable<string> PreTriggers { get; set; }
        /// <summary>
        /// The partition key value of the document, for repositories and bulk operations it will be set from the doucment.
        /// </summary>
        object PartitionKey { get; set; }

        /// <summary>
        /// Gets or sets an absolute expiration date for the cache entry.
        /// </summary>
        public DateTimeOffset? AbsoluteExpiration { get; set; }

        /// <summary>
        /// Gets or sets an absolute expiration time, relative to now.
        /// </summary>
        public TimeSpan? AbsoluteExpirationRelativeToNow { get; set; }

        /// <summary>
        /// Gets or sets how long a cache entry can be inactive (e.g. not accessed) before
        /// it will be removed. This will not extend the entry lifetime beyond the absolute
        /// expiration (if set).
        /// </summary>
        public TimeSpan? SlidingExpiration { get; set; }
        CacheOption Cache { get; set; }
    }
}