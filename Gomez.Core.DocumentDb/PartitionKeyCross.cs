﻿namespace Gomez.Core.DocumentDb
{
    /// <summary>
    /// Can be used to pass as a object if document 
    /// provider support collection without parition keys
    /// </summary>
    public struct PartitionKeyCross
    {
        public static PartitionKeyCross Default => new PartitionKeyCross();

        public override bool Equals(object obj)
        {
            return GetType() == obj.GetType();
        }

        public override int GetHashCode()
        {
            return 1;
        }

        public static bool operator ==(PartitionKeyCross left, PartitionKeyCross right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(PartitionKeyCross left, PartitionKeyCross right)
        {
            return !(left == right);
        }
    }
}
