﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Gomez.Core.DocumentDb
{
    public class CacheHelper
    {
        private readonly IDistributedCache _cache;
        private readonly string _prefix;

        public CacheHelper(IDistributedCache cache, string prefix)
        {
            _cache = cache;
            _prefix = prefix;
        }

        private string GetKey(string collectionName, string id, object partitionKey)
        {
            return $"{_prefix}_{collectionName}_{id}{partitionKey?.ToString() ?? ""}";
        }

        public async Task<T> GetAsync<T>(string collectionName, string id, object partitionKey) where T : IDocumentEntity
        {
            string key = GetKey(collectionName, id, partitionKey);
            return await GetAsync<T>(key);
        }

        public async Task<T> GetAsync<T>(string key) where T : IDocumentEntity
        {
            if (_cache == null)
            {
                return default;
            }

            string strValue = await _cache.GetStringAsync(key);
            if (!string.IsNullOrEmpty(strValue))
            {
                return JsonConvert.DeserializeObject<T>(strValue);
            }

            return default;
        }

        public async Task SetAsync<T>(
            string collectionName,
            T entity,
            IItemOptions options) where T : IDocumentEntity
        {
            if(entity == null)
            {
                return;
            }

            string key = GetKey(collectionName, entity.Id, options.PartitionKey);
            await SetByKeyAsync(key, entity, options);
        }

        public async Task SetByKeyAsync<T>(string key, T entity, IItemOptions options) where T : IDocumentEntity
        {
            if (_cache == null || entity == null)
            {
                return;
            }

            string strValue = JsonConvert.SerializeObject(entity);
            await _cache.SetStringAsync(key, strValue, CreateOption(options));
        }

        public async Task DeleteAsync(string collectionName, string id, object partitionKey)
        {
            if (_cache == null)
            {
                return;
            }

            string key = GetKey(collectionName, id, partitionKey);
            await _cache.RemoveAsync(key);
        }

        public async Task<bool> SetCTokenAsync(Guid key, string token, IItemOptions options)
        {
            if (_cache == null)
            {
                return false;
            }

            await _cache.SetStringAsync(GetCTokenKey(key), token, CreateOption(options));
            return true;
        }

        public string GetCToken(Guid key)
        {
            if (_cache == null)
            {
                return null;
            }

            return _cache.GetString(GetCTokenKey(key));
        }

        public void DeleteCToken(Guid key)
        {
            if (_cache == null)
            {
                return;
            }

            _cache.Remove(GetCTokenKey(key));
        }

        public Task<string> GetCTokenAsync(Guid key)
        {
            if (_cache == null)
            {
                return Task.FromResult<string>(null);
            }

            return _cache.GetStringAsync(GetCTokenKey(key));
        }

        public Task DeleteCTokenAsync(Guid key)
        {
            if (_cache == null)
            {
                return Task.CompletedTask;
            }

            return _cache.RemoveAsync(GetCTokenKey(key));
        }

        private string GetCTokenKey(Guid key)
        {
            return $"{key}_ctoken";
        }

        private DistributedCacheEntryOptions CreateOption(IItemOptions options)
        {
            if(options == null)
            {
                return new DistributedCacheEntryOptions();
            }

            return new DistributedCacheEntryOptions()
            {
                AbsoluteExpiration = options.AbsoluteExpiration,
                AbsoluteExpirationRelativeToNow = options.AbsoluteExpirationRelativeToNow,
                SlidingExpiration = options.SlidingExpiration
            };
        }
    }
}
