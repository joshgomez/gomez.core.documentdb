﻿using Newtonsoft.Json;
using System;

namespace Gomez.Core.DocumentDb
{
    public class DefaultDocumentEntity : DocumentEntity, IDefaultDocumentEntity
    {
        public DefaultDocumentEntity() : base()
        {
            CreatedAt = DateTime.UtcNow;
        }

        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "ttl")]
        public int? TimeToLive { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }


        [Newtonsoft.Json.JsonConverter(typeof(UnixDateTimeConverter))]
        [Newtonsoft.Json.JsonProperty("_ts")]
        public DateTime UpdatedAt { get; protected set; }

        [JsonProperty("deletedAt")]
        public DateTime? DeletedAt { get; set; }

        [JsonProperty("orderNumber")]
        public int OrderNumber { get; set; }
    }
}
