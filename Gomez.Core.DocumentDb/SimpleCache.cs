﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Gomez.Core.DocumentDb
{
    public class SimpleCache : IDistributedCache, IDisposable
    {
        private IMemoryCache _cache;

        public SimpleCache(IMemoryCache cache)
        {
            _cache = cache;
        }

        public byte[] Get(string key)
        {
            return _cache.Get<byte[]>(key);
        }

        public Task<byte[]> GetAsync(string key, CancellationToken token = default)
        {
            return Task.Run(() => Get(key), token);
        }

        public void Refresh(string key)
        {
            throw new NotImplementedException();
        }

        public Task RefreshAsync(string key, CancellationToken token = default)
        {
            throw new NotImplementedException();
        }

        public void Remove(string key)
        {
            _cache.Remove(key);
        }

        public Task RemoveAsync(string key, CancellationToken token = default)
        {
            return Task.Run(() => Remove(key), token);
        }

        public void Set(string key, byte[] value, DistributedCacheEntryOptions options)
        {
            _cache.Set(key, value);
        }

        public Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token = default)
        {
            return Task.Run(() => Set(key, value, options), token);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && _cache != null)
                {
                    _cache.Dispose();
                    _cache = null;
                }

                disposedValue = true;
            }
        }

        ~SimpleCache()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
