﻿using Newtonsoft.Json;
using System;

namespace Gomez.Core.DocumentDb
{
    public interface IDefaultDocumentEntity : IDocumentEntity, ITypedDocumentEntity, ITtlDocumentEntity
    {
        [JsonProperty("createdAt")]
        DateTime CreatedAt { get; set; }
        /// <summary>
        /// Used for soft deleting
        /// </summary>
        [JsonProperty("deletedAt")]
        DateTime? DeletedAt { get; set; }
        /// <summary>
        /// Can be used for drag and drop sorting
        /// </summary>
        [JsonProperty("orderNumber")]
        int OrderNumber { get; set; }

        /// <summary>
        /// Should be updated on save
        /// </summary>
        [Newtonsoft.Json.JsonConverter(typeof(UnixDateTimeConverter))]
        [Newtonsoft.Json.JsonProperty("_ts")]
        DateTime UpdatedAt { get; }
    }

    public interface ITypedDocumentEntity
    {
        /// <summary>
        /// Used when sharing collection with different document types
        /// </summary>
        [JsonProperty("type")]
        string Type { get; set; }
    }

    public interface ITtlDocumentEntity
    {
        /// <summary>
        /// Tells how long a document should live, may require implementing garbage collection 
        /// if the document provider does not support it as default.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "ttl")]
        int? TimeToLive { get; set; }
    }

    public interface IPartitionedDocumentEntity
    {
        /// <summary>
        /// The key partition key, do not make any cross partition queries other scheduled back-end tasks.
        /// </summary>
        [JsonProperty("partitionKey")]
        string PartitionKey { get; set; }
    }
}