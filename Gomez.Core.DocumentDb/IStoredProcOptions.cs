﻿namespace Gomez.Core.DocumentDb
{
    public interface IStoredProcOptions : IRequestOptions
    {
        object PartitionKey { get; set; }
        bool EnableScriptLogging { get; set; }
    }
}