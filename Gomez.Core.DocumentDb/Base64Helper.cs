﻿using System;
using System.Text;

namespace Gomez.Core.DocumentDb
{
    public static class Base64Helper
    {
        /// <summary>
        /// Decode base64 value passed from a url.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string DecodeUrlQueryParameter(string value)
        {
            string strToken = value;
            if (value.Contains('_') || value.Contains('/'))
            {
                strToken = strToken
                .Replace('_', '/').Replace('-', '+');
                switch (value.Length % 4)
                {
                    case 2: strToken += "=="; break;
                    case 3: strToken += "="; break;
                }
            }

            var byteToken = Convert.FromBase64String(strToken);
            return Encoding.UTF8.GetString(byteToken);
        }
    }
}
