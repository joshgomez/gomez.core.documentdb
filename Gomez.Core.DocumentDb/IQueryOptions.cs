﻿namespace Gomez.Core.DocumentDb
{
    public interface IQueryOptions : IRequestOptions
    {
        bool? EnableScanInQuery { get; set; }
        int? MaxItemCount { get; set; }
        object PartitionKey { get; set; }
        int? ResponseContinuationTokenLimitInKb { get; set; }

        /// <summary>
        /// The Continuation Token
        /// </summary>
        string RequestContinuation { get; set; }
    }
}