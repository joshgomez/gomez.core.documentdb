﻿using System;

namespace Gomez.Core.DocumentDb
{
    [Flags]
    public enum CacheOption
    {
        None = 0,
        Read = 1,
        Write = 2,
        Both = Read | Write
    }
}
