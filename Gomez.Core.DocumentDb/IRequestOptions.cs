﻿namespace Gomez.Core.DocumentDb
{
    public interface IRequestOptions
    {
        string SessionToken { get; set; }
        string IfNoneMatchEtag { get; set; }
        /// <summary>
        /// Etag of the document, on bulk operations just type anything and it will pick the etag.
        /// </summary>
        string IfMatchEtag { get; set; }
    }
}