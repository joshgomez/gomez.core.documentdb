using Gomez.Core.DocumentDb.Cosmos;
using Gomez.Core.DocumentDb.UnitTest.Models;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Scripts;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Gomez.Core.DocumentDb.UnitTest
{
    [TestCaseOrderer("Gomez.Core.DocumentDb.UnitTest.PriorityOrderer", "Gomez.Core.DocumentDb.UnitTest")]
    public class CosmoDbIntegrationTest
    {
        private const string ENDPOINT_URL = "https://localhost:8081";
        private const string AUTHORIZATION_KEY = "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==";
        private const string DATABASE_ID = "CosmosTest";
        private const string COLLECTION_ID = "Test";

        private static IDocumentDbContext CreateContext()
        {
            return new CosmosDbContext(ENDPOINT_URL, AUTHORIZATION_KEY, DATABASE_ID,null, null, null);
        }

        private static IDocumentDbContext CreateContextWithCache()
        {
            var mc = new MemoryCache(new MemoryCacheOptions() { });
            var cache = new SimpleCache(mc);
            return new CosmosDbContext(ENDPOINT_URL, AUTHORIZATION_KEY, DATABASE_ID, new CosmosOptions()
                { UseContinuationTokenCollection = true, ContinuationTokenCollectionName = COLLECTION_ID }, cache, null);
        }

        [Fact, TestPriority(0)]
        public async Task CreateDatabaseAndCollectionStatusCodeCreatedOrOkTrue()
        {
            using var client = new CosmosClient(ENDPOINT_URL, AUTHORIZATION_KEY);
            Database database = await client.CreateDatabaseIfNotExistsAsync(DATABASE_ID);
            var container = await database.CreateContainerIfNotExistsAsync(COLLECTION_ID, "/partitionKey");
            Assert.True(
                container.StatusCode == System.Net.HttpStatusCode.Created ||
                container.StatusCode == System.Net.HttpStatusCode.OK);
        }

        [Fact, TestPriority(10)]
        public async Task<IDocumentEntity> ReadItemNull()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            var result = await repo.ReadAsync(Guid.NewGuid().ToString(), null);

            Assert.Null(result);
            return result;
        }

        [Fact, TestPriority(11)]
        public async Task<IDocumentEntity> ReadItemWithPkNull()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            var result = await repo.ReadAsync(Guid.NewGuid().ToString(), "none-existing-pk");

            Assert.Null(result);
            return result;
        }

        [Fact, TestPriority(12)]
        public async Task<IDocumentEntity> ReadQueryNull()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            var queryOptions = new QueryOptions() { MaxItemCount = 1, PartitionKey = null };
            var query = repo.GetQuery(queryOptions)
                    .Where(l => l.Name == "NotFound");

            var result = await repo.ReadFromQueryAsync(query,queryOptions,new ItemOptions() { });
            Assert.Null(result);
            return result;
        }

        [Fact, TestPriority(20)]
        public async Task<IDocumentEntity> CreateItemNotNull()
        {
            using IDocumentDbContext context = CreateContext();
            var repo = new TestRepository(context, COLLECTION_ID);
            var result = await repo.CreateAsync(new TestEntity()
            {
                Name = "Hello World",
            });

            Assert.NotNull(result);
            Assert.True(result.UpdatedAt > DateTime.UtcNow.Date);
            return result;
        }

        [Fact, TestPriority(30)]
        public async Task<IDocumentEntity> ReadCacheItemNotNull()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            var result = await repo.CreateAsync(new TestEntity()
            {
                Name = "Read Cache Hello World"
            });

            await repo.ReadAsync(result.Id, null);
            result = await repo.ReadAsync(result.Id, null); //We are testing the cache here.

            Assert.NotNull(result);
            return result;
        }

        [Fact, TestPriority(40)]
        public async Task<IDocumentEntity> ReadItemNotNull()
        {
            using IDocumentDbContext context = CreateContext();
            var repo = new TestRepository(context, COLLECTION_ID);
            var result = await repo.CreateAsync(new TestEntity()
            {
                Name = "Read Cache Hello World"
            });

            await repo.ReadAsync(result.Id, null);
            result = await repo.ReadAsync(result.Id, null); //We are testing the cache here.

            Assert.NotNull(result);
            return result;
        }

        [Fact, TestPriority(50)]
        public async Task<IDocumentEntity> DeleteItemEqualStatusNotFound()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            var result = await repo.CreateAsync(new TestEntity()
            {
                Name = "Delete Hello World"
            });

            await repo.DeleteAsync(result.Id, null);

            try
            {
                result = await repo.ReadAsync(result.Id, null);
            }
            catch (CosmosException ex)
            {
                Assert.Equal(System.Net.HttpStatusCode.NotFound, ex.StatusCode);
            }

            return result;
        }

        [Fact, TestPriority(60)]
        public async Task QueryItemEqualNameQueryFoobar()
        {
            using IDocumentDbContext context = CreateContext();
            var repo = new TestRepository(context, COLLECTION_ID);
            await repo.CreateAsync(new TestEntity()
            {
                Name = "Query Hello World"
            });

            await repo.CreateAsync(new TestEntity()
            {
                Name = "Query Foobar"
            });

            var result = await repo.GetNameByLikeAsync("Foo");
            Assert.Equal("Query Foobar", result);
        }

        [Fact, TestPriority(70)]
        public async Task TestContinuationResults()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            await repo.CreateAsync(new TestEntity()
            {
                Name = "Item1 pt1",
                PartitionKey = "1"
            });

            await repo.CreateAsync(new TestEntity()
            {
                Name = "Item 2 pt2",
                DeletedAt = new System.DateTime(2020, 03, 15),
                PartitionKey = "2"
            });

            await repo.CreateAsync(new TestEntity()
            {
                Name = "Item 3 pt2",
                PartitionKey = "2"
            });

            await repo.CreateAsync(new TestEntity()
            {
                Name = "Item 4 pt2",
                PartitionKey = "2"
            });

            await repo.CreateAsync(new TestEntity()
            {
                Name = "Item 5 pt2",
                PartitionKey = "2"
            });

            await repo.CreateAsync(new TestEntity()
            {
                Name = "Item 5 pt2",
            });

            var result = await repo.GetContinuedListAsync(new QueryOptions()
            {
                MaxItemCount = 2,
                PartitionKey = PartitionKeyCross.Default
            });

            Assert.Equal(2, result.Documents.Count);

            var result2 = await repo.GetContinuedListAsync(new QueryOptions()
            {
                MaxItemCount = 2,
                PartitionKey = PartitionKeyCross.Default,
                RequestContinuation = result.ContinuesToken
            });

            Assert.Equal(2, result2.Documents.Count);
        }

        [Fact, TestPriority(80)]
        public async Task QueryCachableItemEqualNameQueryFoobar()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            await repo.UpsertAsync(new TestEntity()
            {
                Id = "94f93761-0d7f-4306-a680-6f1766b41841",
                Name = "Query Hello World"
            });

            await repo.UpsertAsync(new TestEntity()
            {
                Id = "391ca852-63bb-4a80-92ba-d5550e5d94b5",
                Name = "Query Foobar"
            });

            var result = await repo.ReadFromQueryAsync(x => x.Name == "Query Foobar");
            Assert.Equal("Query Foobar", result.Name);

            result = await repo.ReadFromQueryAsync(x => x.Name == "Query Foobar");
            Assert.Equal("Query Foobar", result.Name);
        }

        [Fact, TestPriority(90)]
        public async Task BulkInsertEqualEmptyResponse()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            var toBeInserted = new List<TestEntity>()
            {
                new TestEntity()
                {
                    Name = "Bulk item 1",
                    PartitionKey="bulk"
                },
                new TestEntity()
                {
                    Name = "Bulk item 2",
                    PartitionKey="bulk"
                }
            };

            var results = await repo.BulkInsertAsync(toBeInserted);
            Assert.DoesNotContain(results, x => x.Status == System.Net.HttpStatusCode.BadRequest);
        }

        [Fact, TestPriority(100)]
        public async Task ExecuteSqlEqualNotNull()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            await repo.CreateAsync(new TestEntity()
            {
                Name = "Sql item 1",
                PartitionKey = "sql"
            });

            var (resource, _) = await repo.ExecuteSqlAsync(
                "SELECT * FROM c WHERE c.name = @name",
                PartitionKeyCross.Default,
                new Dictionary<string, object>
                {
                    { "@name", "Sql item 1" }
                }
            );

            Assert.NotNull(resource.FirstOrDefault());
        }

        private async Task<string> CreateStoredProcDocExistsAsync()
        {
            using var client = new CosmosClient(ENDPOINT_URL, AUTHORIZATION_KEY);
            Database database = client.GetDatabase(DATABASE_ID);
            var container = database.GetContainer(COLLECTION_ID);

            string sprocBody = @"function simple(prefix, postfix)
            {
                var collection = getContext().getCollection();

                // Query documents and take 1st item.
                var isAccepted = collection.queryDocuments(
                collection.getSelfLink(),
                'SELECT * FROM root r',
                function(err, feed, options) {
                    if (err)throw err;

                    // Check the feed and if it's empty, set the body to 'no docs found',
                    // Otherwise just take 1st element from the feed.
                    if (!feed || !feed.length) getContext().getResponse().setBody(""no docs found"");
                    else getContext().getResponse().setBody(""docs found"");
                });

                if (!isAccepted) throw new Error(""The query wasn't accepted by the server. Try again/use continuation token between API and script."");
            }";


            Scripts scripts = container.Scripts;
            string sprocId = "docsExists";
            var proc = await scripts.CreateStoredProcedureAsync(
                new StoredProcedureProperties() { Body = sprocBody, Id = sprocId });
            return proc.Resource.Id;
        }

        [Fact, TestPriority(110)]
        public async Task ExecuteSprocEqualTextDocsFound()
        {
            var sprocId = await CreateStoredProcDocExistsAsync();

            using IDocumentDbContext context = CreateContextWithCache();
            var result = await context.ExecuteStoredProcedureAsync<string>(COLLECTION_ID, sprocId, new StoredProcOptions());
            Assert.Equal("docs found", result);
        }

        private async Task<string> CreateUdfTestAsync()
        {
            using var client = new CosmosClient(ENDPOINT_URL, AUTHORIZATION_KEY);
            Database database = client.GetDatabase(DATABASE_ID);
            var container = database.GetContainer(COLLECTION_ID);

            var udf = new UserDefinedFunctionProperties
            {
                Id = "toLowerUdfTest",
                Body = @"function (input) {
                      return input.toLowerCase();
                   };",
            };

            UserDefinedFunctionResponse response;
            try
            {
                var existingUdf = await container.Scripts.ReadUserDefinedFunctionAsync(udf.Id);
                if (existingUdf != null)
                {
                    response = await container.Scripts.ReplaceUserDefinedFunctionAsync(udf);
                    return response.Resource.Id;
                }
            }
            catch (CosmosException ex)
            {
                if (ex.StatusCode != System.Net.HttpStatusCode.NotFound)
                {
                    throw;
                }
            }

            response = await container.Scripts.CreateUserDefinedFunctionAsync(udf);
            return response.Resource.Id;
        }


        [Fact, TestPriority(120)]
        public async Task ExecuteUdfEqualTrue()
        {
            var udfId = await CreateUdfTestAsync();
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            var upperCaseItem = await repo.CreateAsync(new TestEntity()
            {
                Name = "UPPERCASE ITEM",
                PartitionKey = "udf"
            });



            var (resource, _) = await context.ExecuteSqlAsync<PrimitiveValue<string>>(COLLECTION_ID, $"SELECT udf.{udfId}(c.name) FROM c",
                new QueryOptions()
                {
                    PartitionKey = "udf"
                });

            var result = resource.FirstOrDefault();
            Assert.Equal(upperCaseItem.Name.ToLower(), result.Value);
        }

        [Fact, TestPriority(130)]
        public async Task ExecuteBatchEqualSuccess()
        {
            var pk = "Transaction";

            using IDocumentDbContext context = CreateContextWithCache();
            var t = context.CreateTransaction(COLLECTION_ID, pk);
            t.Create(new TestEntity() { PartitionKey = pk, Name = "t item 1" }, new TransactionOptions() { });
            t.Create(new TestEntity() { PartitionKey = pk, Name = "t item 2" }, new TransactionOptions() { });
            var result = context.ExecuteTransactionAsync(t);
            await foreach (var item in result)
            {
                Assert.Equal(HttpStatusCode.Created, item.Status);
            }
        }

        [Fact, TestPriority(140)]
        public async Task BulkUpsertEqualEmptyResponse()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);
            var toBeInserted = new List<TestEntity>()
            {
                new TestEntity()
                {
                    Id = "7644e602-dd07-4489-ab33-3dfd8eb3920a",
                    Name = "Bulk item",
                    PartitionKey="bulk"
                },
                new TestEntity()
                {
                    Id = "7644e602-dd07-4489-ab33-3dfd8eb3920a",
                    Name = "Bulk item updated",
                    PartitionKey="bulk"
                }
            };

            var results = await repo.BulkUpsertAsync(toBeInserted);
            Assert.DoesNotContain(results, x => x.Status == System.Net.HttpStatusCode.BadRequest);
        }

        [Fact, TestPriority(150)]
        public async Task CountEqualTwo()
        {
            using IDocumentDbContext context = CreateContextWithCache();
            var repo = new TestRepository(context, COLLECTION_ID);

            await repo.CreateAsync(new TestEntity()
            {
                Name = "Count Item 1",
                PartitionKey = "Count"
            });

            await repo.CreateAsync(new TestEntity()
            {
                Name = "Count Item 2",
                PartitionKey = "Count"
            });

            
            Assert.Equal(2,await repo.CountInPartitionCountAsync());
        }

        [Fact, TestPriority(1000)]
        public async Task DeleteDatabaseEqualStatusNoContent()
        {
            using var client = new CosmosClient(ENDPOINT_URL, AUTHORIZATION_KEY);
            Database database = client.GetDatabase(DATABASE_ID);
            var response = await database.DeleteAsync();
            Assert.Equal(System.Net.HttpStatusCode.NoContent, response.StatusCode);
        }
    }
}
