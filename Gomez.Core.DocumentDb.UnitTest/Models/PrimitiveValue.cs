﻿using Newtonsoft.Json;

namespace Gomez.Core.DocumentDb.UnitTest.Models
{
    public class PrimitiveValue<T>
    {
        [JsonProperty("$1")]
        public T Value { get; set; }
    }
}
