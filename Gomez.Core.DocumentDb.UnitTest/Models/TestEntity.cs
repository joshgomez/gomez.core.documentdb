﻿using Newtonsoft.Json;

namespace Gomez.Core.DocumentDb.UnitTest.Models
{
    public class TestEntity : DefaultDocumentEntity
    {
        public TestEntity() : base()
        {

        }


        [JsonProperty("partitionKey")]
        public string PartitionKey { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
