﻿using System.Linq;
using System.Threading.Tasks;

namespace Gomez.Core.DocumentDb.UnitTest.Models
{
    class TestRepository : DocumentRepository<TestEntity>
    {
        public TestRepository(IDocumentDbContext context, string collectionName) : base(context, collectionName, x => x.PartitionKey)
        {
        }

        public async Task<string> GetNameByLikeAsync(string searchValue)
        {
            var query = GetQuery().Where(x => x.Name.Contains(searchValue))
                .Select(x => x.Name);

            return await _context.FirstOrDefaultAsync(query);
        }

        public async Task<IContinuedList<TestEntity>> GetContinuedListAsync(IQueryOptions options)
        {
            var query = GetQuery(options).OrderByDescending(x => x.UpdatedAt);
            return await _context.ToContinuedListAsync(query);
        }

        public async Task<int> CountInPartitionCountAsync()
        {
            var query = GetQuery(new QueryOptions() { PartitionKey = "Count" });
            return await _context.CountAsync(query);
        }
    }
}
