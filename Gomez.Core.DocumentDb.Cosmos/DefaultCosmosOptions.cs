﻿using Microsoft.Extensions.Configuration;
using System;

namespace Gomez.Core.DocumentDb.Cosmos
{
    public class DefaultCosmosOptions : ICosmosOptions
    {
        public DefaultCosmosOptions()
        {

        }

        public DefaultCosmosOptions(ICosmosOptions options)
        {
            AbsoluteExpirationRelativeToNow = options.AbsoluteExpirationRelativeToNow;
            SlidingExpiration = options.SlidingExpiration;

            ContinuationTokenCollectionName = options.ContinuationTokenCollectionName;
            UseContinuationTokenCollection = options.UseContinuationTokenCollection;
        }

        public DefaultCosmosOptions(IConfiguration configuration)
        {
            var cTokenCollName = configuration["CosmosDb:ContinuationTokenCollectionName"];
            ContinuationTokenCollectionName = string.IsNullOrEmpty(cTokenCollName) ?
                ContinuationTokenCollectionName : cTokenCollName;

            if (bool.TryParse(configuration["CosmosDb:UseContinuationTokenCollection"],
                out bool useContinuationTokenCollection))
            {
                UseContinuationTokenCollection = useContinuationTokenCollection;
            }

            if (int.TryParse(configuration["CosmosDb:MaxContinuationTokenLengthBeforeCache"],
                out int maxContinuationTokenLengthBeforeCache))
            {
                MaxContinuationTokenLengthBeforeCache = maxContinuationTokenLengthBeforeCache;
            }

            if (int.TryParse(configuration["CosmosDb:AbsoluteExpirationRelativeToNowSeconds"],
                out int absoluteExpirationRelativeToNow))
            {
                AbsoluteExpirationRelativeToNow = new TimeSpan(0,0, absoluteExpirationRelativeToNow);
            }

            if (int.TryParse(configuration["CosmosDb:SlidingExpirationSeconds"],
                out int slidingExpiration))
            {
                SlidingExpiration = new TimeSpan(0, 0, slidingExpiration);
            }
        }

        public TimeSpan? AbsoluteExpirationRelativeToNow { get; set; }
        public TimeSpan? SlidingExpiration { get; set; }

        public bool UseContinuationTokenCollection { get; set; }
        public string ContinuationTokenCollectionName { get; set; } = "tokens";
        public int MaxContinuationTokenLengthBeforeCache { get; set; } = 255;
        public LogType LogTypes { get; set; }
    }
}
