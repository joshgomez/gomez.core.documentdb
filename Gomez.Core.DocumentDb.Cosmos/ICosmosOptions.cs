﻿using System;

namespace Gomez.Core.DocumentDb.Cosmos
{
    public interface ICosmosOptions
    {
        TimeSpan? AbsoluteExpirationRelativeToNow { get; set; }
        TimeSpan? SlidingExpiration { get; set; }

        string ContinuationTokenCollectionName { get; set; }
        bool UseContinuationTokenCollection { get; set; }
        int MaxContinuationTokenLengthBeforeCache { get; set; }
        LogType LogTypes { get; set; }
    }
}