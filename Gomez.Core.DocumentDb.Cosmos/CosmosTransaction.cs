﻿using Microsoft.Azure.Cosmos;
using System;

namespace Gomez.Core.DocumentDb.Cosmos
{
    public class CosmosTransaction : ITransaction<TransactionalBatch>
    {
        public TransactionalBatch Context { get; private set; }

        public CosmosTransaction(TransactionalBatch context)
        {
            Context = context;
        }

        public ITransaction Create<T>(T item, ITransactionOptions options) where T : IDocumentEntity
        {
            Context = Context.CreateItem(item, CreateOptions(options));
            return this;
        }

        public ITransaction UpsertItem<T>(T item, ITransactionOptions options) where T : IDocumentEntity
        {
            Context = Context.UpsertItem(item, CreateOptions(options));
            return this;
        }

        public ITransaction ReadItem(string id, ITransactionOptions options)
        {
            Context = Context.ReadItem(id, CreateOptions(options));
            return this;
        }

        public ITransaction DeleteItem(string id, ITransactionOptions options)
        {
            Context = Context.DeleteItem(id, CreateOptions(options));
            return this;
        }

        public ITransaction ReplaceItem<T>(T item, ITransactionOptions options) where T : IDocumentEntity
        {
            Context = Context.ReplaceItem(item.Id, item, CreateOptions(options));
            return this;
        }

        private TransactionalBatchItemRequestOptions CreateOptions(ITransactionOptions options)
        {
            if (options is null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            var queryOptions = new TransactionalBatchItemRequestOptions();
            queryOptions.EnableContentResponseOnWrite ??= options.EnableContentResponseOnWrite;
            queryOptions.IfNoneMatchEtag ??= options.IfNoneMatchEtag;
            queryOptions.IfMatchEtag ??= options.IfMatchEtag;
            return queryOptions;
        }
    }
}
