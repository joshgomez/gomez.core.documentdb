﻿using Gomez.Core.DocumentDb.Cosmos.AsyncBridge;
using Gomez.Core.DocumentDb.Cosmos.Models;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using Microsoft.Azure.Cosmos.Scripts;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Gomez.Core.DocumentDb.Cosmos
{
    /// <summary>
    /// The context should not be used directly
    /// </summary>
    public class CosmosDbContext : DocumentDbContext<CosmosDbContext>, IDocumentDbContext
    {
        private readonly CacheHelper _cacheHelper;

        private readonly ConcurrentDictionary<string, Container> _containers;

        private readonly Database _db;

        private readonly ICosmosOptions _options = new DefaultCosmosOptions();

        private readonly bool disposingClient = true;

        private CosmosClient _client;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpointUrl"></param>
        /// <param name="authToken"></param>
        /// <param name="databaseName"></param>
        /// <param name="options"></param>
        /// <param name="cache">Any cache provider that implements IDistributedCache can be used.</param>
        public CosmosDbContext(string endpointUrl,
                               string authToken,
                               string databaseName,
                               CosmosClientOptions options,
                               IDistributedCache cache,
                               ILogger<CosmosDbContext> logger) : this(logger, cache, endpointUrl, databaseName, options)
        {
            _client = new CosmosClient(endpointUrl, authToken, options);
            _db = _client.GetDatabase(databaseName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="options"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        public CosmosDbContext(DbConnectionString connectionString,
                               CosmosClientOptions options,
                               IDistributedCache cache,
                               ILogger<CosmosDbContext> logger) : this(logger,
                                   cache,
                                   connectionString.ServiceEndpoint.ToString(),
                                   connectionString.Database,
                                   options)
        {
            _client = new CosmosClient(connectionString.ServiceEndpoint.ToString(), connectionString.AuthKey, options);
            _db = _client.GetDatabase(connectionString.Database);
        }

        /// <summary>
        /// Create a context where the client is injected, the client disposing is not handled.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="databaseName"></param>
        /// <param name="cache"></param>
        public CosmosDbContext(
            CosmosClient client,
            string databaseName,
            IDistributedCache cache,
            ILogger<CosmosDbContext> logger,
            ICosmosOptions options = null) : this(logger, cache, databaseName, options)
        {
            _client = client;
            _db = _client.GetDatabase(databaseName);
            disposingClient = false;
        }

        /// <summary>
        /// Create a context where the client is injected, the client disposing is not handled.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="client"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="options"></param>
        public CosmosDbContext(
            DbConnectionString connectionString,
            CosmosClient client,
            IDistributedCache cache,
            ILogger<CosmosDbContext> logger,
            ICosmosOptions options = null) : this(logger, cache, connectionString.Database, options)
        {
            _client = client;
            _db = _client.GetDatabase(connectionString.Database);
            disposingClient = false;
        }

        /// <summary>
        /// Create a context where the client is injected, the client disposing is not handled.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="databaseName"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="configuration"></param>
        public CosmosDbContext(
            CosmosClient client,
            string databaseName,
            IDistributedCache cache,
            ILogger<CosmosDbContext> logger,
            IConfiguration configuration) : this(logger, cache, databaseName, new DefaultCosmosOptions(configuration))
        {
            _client = client;
            _db = _client.GetDatabase(databaseName);
            disposingClient = false;
        }

        protected CosmosDbContext(
            ILogger<CosmosDbContext> logger,
            IDistributedCache cache, string databaseName,
            ICosmosOptions options) : base(databaseName, logger)
        {
            if (options != null)
            {
                _options = options;
            }

            _containers = new ConcurrentDictionary<string, Container>();
            Cache = cache;
            _cacheHelper = new CacheHelper(Cache, "cosmodb");
        }

        protected CosmosDbContext(
            ILogger<CosmosDbContext> logger,
            IDistributedCache cache, string endpointUrl, string databaseName,
            CosmosClientOptions options) : base(endpointUrl, databaseName, logger)
        {
            if (options is CosmosOptions)
            {
                _options = options as CosmosOptions;
            }

            _containers = new ConcurrentDictionary<string, Container>();
            Cache = cache;
            _cacheHelper = new CacheHelper(Cache, "cosmodb");
        }

        delegate Task<ItemResponse<T>> BulkMethod<T>(T item,
                                                                                                                    PartitionKey? partitionKey = null,
            ItemRequestOptions requestOptions = null,
            CancellationToken cancellationToken = default);
        public IDistributedCache Cache { get; }
        public async Task<List<BulkResponse<T>>> BulkDeleteAsync<T>(
            string collectionName,
            IEnumerable<T> entities,
            IItemOptions options,
            Func<T, object> getPkFunc) where T : IDocumentEntity
        {
            var ret = new List<BulkResponse<T>>();
            var requestOptions = CreateOptions(options);
            var pk = CreatePartitionKey(options.PartitionKey);
            var container = GetContainer(collectionName);
            List<Task> tasks = new List<Task>();
            foreach (var entity in entities)
            {
                options.PartitionKey = getPkFunc == null ? options.PartitionKey : getPkFunc(entity);
                pk = getPkFunc != null ? CreatePartitionKey(options.PartitionKey) : pk;
                if (!String.IsNullOrEmpty(options.IfMatchEtag))
                {
                    requestOptions = CreateMustMatchOptions(entity.ETag ?? options.IfMatchEtag, requestOptions);
                }

                tasks.Add(container.DeleteItemAsync<T>(entity.Id, pk, requestOptions)
                    .ContinueWith(async (Task<ItemResponse<T>> task) =>
                    {
                        var response = await task;
                        LogResponse(response, collectionName, LogCrudType.BulkDelete, entity);
                        if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NoContent)
                        {
                            await _cacheHelper.DeleteAsync(collectionName, entity.Id, options.PartitionKey);
                            ret.Add(new BulkResponse<T>
                            {
                                Document = entity,
                                Status = response.StatusCode
                            });
                            return;
                        }

                        ret.Add(new BulkResponse<T>
                        {
                            Document = entity,
                            Status = response.StatusCode,
                            Message = response.Diagnostics.ToString()
                        });
                    }));
            }

            // Wait until all are done
            await Task.WhenAll(tasks);
            return ret;
        }

        public async Task<List<BulkResponse<T>>> BulkInsertAsync<T>(string collectionName, IEnumerable<T> entities, IItemOptions options, Func<T, object> getPkFunc) where T : IDocumentEntity
        {
            var container = GetContainer(collectionName);
            return await BulkOperationAsync(
                collectionName,
                entities,
                options,
                container.CreateItemAsync,
                getPkFunc,
                LogCrudType.BulkInsert);
        }

        public async Task<List<BulkResponse<T>>> BulkReplaceAsync<T>(string collectionName, IEnumerable<T> entities, IItemOptions options, Func<T, object> getPkFunc) where T : IDocumentEntity
        {
            Task<ItemResponse<T>> replaceItemAsync(
                T item,
                PartitionKey? pk = null,
                ItemRequestOptions options = null,
                CancellationToken cToken = default)
            {
                var container = GetContainer(collectionName);
                return container.ReplaceItemAsync(item, item.Id, pk, options, cToken);
            }

            return await BulkOperationAsync(
                collectionName,
                entities,
                options,
                replaceItemAsync,
                getPkFunc,
                LogCrudType.BulkReplace);
        }

        public async Task<List<BulkResponse<T>>> BulkUpsertAsync<T>(string collectionName, IEnumerable<T> entities, IItemOptions options, Func<T, object> getPkFunc) where T : IDocumentEntity
        {
            var container = GetContainer(collectionName);
            return await BulkOperationAsync(
                collectionName,
                entities,
                options,
                container.UpsertItemAsync,
                getPkFunc,
                LogCrudType.BulkUpsert);
        }

        public async Task<int> CountAsync<T>(IQueryable<T> query)
        {
            try
            {
                var cacheKey = ExpressionKeyGenerator.GetKey(query.Expression);
                var response = await query.CountAsync();
                LogResponse(response, cacheKey + ":Count");
                return response.Resource;
            }
            catch (Exception ex)
            {
                LogExceptions(ex, nameof(CountAsync), ("Query", query.ToString()));
                throw;
            }
        }

        public async Task<T> CreateAsync<T>(string collectionName, T entity, IItemOptions options) where T : IDocumentEntity
        {
            var container = GetContainer(collectionName);
            var requestOptions = CreateOptions(options);
            var pk = CreateNullablePartitionKey(options.PartitionKey);
            try
            {
                var response = await container.CreateItemAsync(entity, pk, requestOptions);
                LogResponse(response, collectionName, LogCrudType.Create, entity);
                if (options.Cache.HasFlag(CacheOption.Write))
                {
                    await _cacheHelper.SetAsync(collectionName, response.Resource, options);
                }

                return response.Resource;
            }
            catch (Exception ex)
            {
                LogException(ex, collectionName, entity, nameof(CreateAsync));
                throw;
            }
        }

        public ITransaction CreateTransaction(string collectionName, object partitionKey)
        {
            var container = GetContainer(collectionName);
            var tBatch = container.CreateTransactionalBatch(CreatePartitionKey(partitionKey));
            return new CosmosTransaction(tBatch);
        }

        public async Task DeleteAsync<T>(string collectionName, string id, IItemOptions options, string eTag = "") where T : IDocumentEntity
        {
            var container = GetContainer(collectionName);
            var requestOptions = CreateOptions(options);
            var pk = CreatePartitionKey(options.PartitionKey);
            await _cacheHelper.DeleteAsync(collectionName, id, options.PartitionKey);
            try
            {
                ItemResponse<T> response;
                if (!string.IsNullOrEmpty(eTag))
                {
                    response = await container.DeleteItemAsync<T>(id, pk, CreateMustMatchOptions(eTag, requestOptions));
                    LogResponse(response, collectionName, id, LogCrudType.Delete);
                    return;
                }

                response = await container.DeleteItemAsync<T>(id, pk, requestOptions);
                LogResponse(response, collectionName, id, LogCrudType.Delete);
            }
            catch (Exception ex)
            {
                LogException(ex, collectionName, id, nameof(DeleteAsync));
                throw;
            }
        }

        public async Task<(IEnumerable<T> resource, string continuationToken)> ExecuteSqlAsync<T>(
            string collectionName,
            string sqlText,
            IQueryOptions options,
            Dictionary<string, object> parameters = null)
        {
            var container = GetContainer(collectionName);
            var requestOptions = CreateOptions(options);
            var queryDef = new QueryDefinition(sqlText);
            if (parameters != null && parameters.Count > 0)
            {
                foreach (var parameter in parameters)
                {
                    queryDef = queryDef.WithParameter(parameter.Key, parameter.Value);
                }
            }

            requestOptions.PartitionKey = CreateNullablePartitionKey(options.PartitionKey);
            var iterator = container.GetItemQueryIterator<T>(queryDef, options.RequestContinuation, requestOptions);
            if (iterator.HasMoreResults)
            {
                var response = await ReadNextAsync(iterator, queryDef);
                return (response.AsEnumerable(),response.ContinuationToken);
            }

            return (Enumerable.Empty<T>(), null);
        }

        public async Task<T> ExecuteStoredProcedureAsync<T>(
            string collectionName,
            string sprocId,
            IStoredProcOptions options,
            params object[] parameters)
        {
            var container = GetContainer(collectionName);
            var pk = CreatePartitionKey(options.PartitionKey);
            var requestOptions = CreateOptions(options);
            try
            {
                var sprocResponse = await container.Scripts.ExecuteStoredProcedureAsync<T>(
                                        sprocId,
                                        pk,
                                        parameters,
                                        requestOptions);

                LogResponse<T>(sprocResponse, "", ("SprocId", sprocId));
                return sprocResponse.Resource;
            }
            catch(Exception ex)
            {
                LogException(ex, collectionName, sprocId, nameof(ExecuteStoredProcedureAsync));
                throw;
            }
        }

        public IAsyncEnumerable<TransactionResponse> ExecuteTransactionAsync(ITransaction transaction)
        {
            if (!(transaction is ITransaction<TransactionalBatch> underlyingTransaction))
            {
                throw new ArgumentException("The underlaying type is wrong.", nameof(transaction));
            }

            return ExecuteTransactionInternalAsync(underlyingTransaction);
        }

        public Task<T> FirstOrDefaultAsync<T>(IQueryable<T> query)
        {
            return FirstOrDefaultAsync(query, null);
        }

        public async Task<T> FirstOrDefaultAsync<T>(IQueryable<T> query, Expression<Func<T, bool>> predicate)
        {
            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            query = query.Take(1);
            var iterator = query.ToFeedIterator();
            if (iterator.HasMoreResults)
            {
                var response = await ReadNextAsync(iterator, query);
                return response.FirstOrDefault();
            }

            return default;
        }

        public IQueryable<T> GetQuery<T>(string collectionName)
        {
            var container = GetContainer(collectionName);
            return container.GetItemLinqQueryable<T>();
        }

        public IQueryable<T> GetQuery<T>(string collectionName, IQueryOptions options)
        {
            var container = GetContainer(collectionName);
            QueryRequestOptions queryOptions = CreateOptions(options);
            if (!string.IsNullOrEmpty(options.RequestContinuation))
            {
                SetCTokenFromCache(ref options);
                options.RequestContinuation =
                    Base64Helper.DecodeUrlQueryParameter(options.RequestContinuation);
            }

            return container.GetItemLinqQueryable<T>(continuationToken: options.RequestContinuation, requestOptions: queryOptions);
        }

        public async Task<IQueryable<T>> GetQueryAsync<T>(string collectionName, IQueryOptions options)
        {
            var container = GetContainer(collectionName);
            QueryRequestOptions queryOptions = CreateOptions(options);
            if (!string.IsNullOrEmpty(options.RequestContinuation))
            {
                options = await SetCTokenFromCacheAsync(options);
                options.RequestContinuation =
                    Base64Helper.DecodeUrlQueryParameter(options.RequestContinuation);
            }

            return container.GetItemLinqQueryable<T>(continuationToken: options.RequestContinuation, requestOptions: queryOptions);
        }

        public async Task<T> MaxAsync<T>(IQueryable<T> query)
        {
            try
            {
                var cacheKey = ExpressionKeyGenerator.GetKey(query.Expression);
                var response = await query.MaxAsync();
                LogResponse(response, cacheKey + ":Max");
                return response.Resource;
            }
            catch (Exception ex)
            {
                LogExceptions(ex, nameof(MaxAsync), ("Query", query.ToString()));
                throw;
            }
        }

        public async Task<T> MinAsync<T>(IQueryable<T> query)
        {
            try
            {
                var cacheKey = ExpressionKeyGenerator.GetKey(query.Expression);
                var response = await query.MinAsync();
                LogResponse(response, cacheKey + ":Min");
                return response.Resource;
            }
            catch (Exception ex)
            {
                LogExceptions(ex, nameof(MinAsync), ("Query", query.ToString()));
                throw;
            }
        }

        public async Task<T> ReadAsync<T>(string collectionName, string id, IItemOptions options) where T : IDocumentEntity
        {
            var container = GetContainer(collectionName);
            var entity = options.Cache.HasFlag(CacheOption.Read)
                ? await _cacheHelper.GetAsync<T>(collectionName, id, options.PartitionKey) : default;
            var requestOptions = CreateOptions(options);
            ItemResponse<T> response;
            if (entity != null)
            {
                try
                {
                    //Cache was found but we are checking the database if etag is matching.
                    response = await container.ReadItemAsync<T>(id, CreatePartitionKey(options.PartitionKey),
                        CreateNotMatchOptions(entity.ETag, requestOptions));
                    LogResponse(response, collectionName, LogCrudType.Read, entity);
                }
                catch (CosmosException ex)
                {
                    if (!IsNotModified(ex) && !IsNotFound(ex))
                    {
                        LogException(ex, collectionName, entity, nameof(ReadAsync));
                        throw;
                    }

                    //We are using the cache.
                    return entity;
                }
                catch (Exception ex)
                {
                    LogException(ex, collectionName, entity, nameof(ReadAsync));
                    throw;
                }

                //Etag was not matching so we are updating the cache.
                if (options.Cache.HasFlag(CacheOption.Write))
                {
                    await _cacheHelper.SetAsync(collectionName, response.Resource, options);
                }

                return response.Resource;
            }

            //No cach was found so we are gettin the item directly.
            try
            {
                response = await container.ReadItemAsync<T>(id, CreatePartitionKey(options.PartitionKey), requestOptions);
                LogResponse(response, collectionName, LogCrudType.Read, entity);
                if (options.Cache.HasFlag(CacheOption.Write))
                {
                    await _cacheHelper.SetAsync(collectionName, response.Resource, options);
                }

                return response.Resource;
            }
            catch (CosmosException ex) when (IsNotFound(ex))
            {
                return default;
            }
            catch (Exception ex)
            {
                LogException(ex, collectionName, entity, nameof(ReadAsync));
                throw;
            }
        }

        public Task<T> ReadFromQueryAsync<T>(string collectionName, Expression<Func<T, bool>> predicate,
            IQueryOptions queryOptions,
            IItemOptions itemOptions,
            Func<T, object> partitionKeyFunc) where T : IDocumentEntity
        {
            ThrowIfNoCache(itemOptions, nameof(ReadFromQueryAsync));

            var query = GetQuery<T>(collectionName, queryOptions)
                .Where(predicate)
                .Take(1);

            return ReadFromQueryAsync(collectionName, query, queryOptions, itemOptions, partitionKeyFunc);
        }

        public async Task<T> ReadFromQueryAsync<T>(string collectionName, IQueryable<T> query,
            IQueryOptions queryOptions,
            IItemOptions itemOptions,
            Func<T, object> partitionKeyFunc) where T : IDocumentEntity
        {
            ThrowIfNoCache(itemOptions, nameof(ReadFromQueryAsync));

            var cacheKey = ExpressionKeyGenerator.GetKey(query.Expression);
            var entity = await _cacheHelper.GetAsync<T>(cacheKey);
            //Check if the entity should exists as it handle delete.
            T validationEntity = default;
            itemOptions.PartitionKey ??= queryOptions.PartitionKey;
            if (entity != null)
            {
                itemOptions.PartitionKey = partitionKeyFunc != null ? partitionKeyFunc(entity) : itemOptions.PartitionKey;
                validationEntity = await _cacheHelper.GetAsync<T>(collectionName, entity.Id, itemOptions.PartitionKey);
            }

            if (entity != null && validationEntity != null)
            {
                entity = await ReadAsync<T>(collectionName, entity.Id, itemOptions);
                if (entity != null)
                {
                    await _cacheHelper.SetByKeyAsync(cacheKey, entity, itemOptions);
                }

                return entity;
            }

            var iterator = query.ToFeedIterator<T>();
            if (iterator.HasMoreResults)
            {
                var response = await ReadNextAsync(iterator, query);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    entity = response.FirstOrDefault();
                    await _cacheHelper.SetAsync(collectionName, entity, itemOptions);
                    await _cacheHelper.SetByKeyAsync(cacheKey, entity, itemOptions);
                    return entity;
                }
                else if (!String.IsNullOrEmpty(entity?.Id))
                {
                    //Only required to delete the "validation entity"
                    await _cacheHelper.DeleteAsync(collectionName, entity.Id, itemOptions.PartitionKey);
                }
            }

            return default;
        }

        public async Task<T> ReplaceAsync<T>(string collectionName, T entity, IItemOptions options) where T : IDocumentEntity
        {
            var container = GetContainer(collectionName);
            var requestOptions = CreateOptions(options);
            var pk = CreateNullablePartitionKey(options.PartitionKey);
            try
            {
                var response = await container.ReplaceItemAsync(entity, entity.Id, pk, requestOptions);
                LogResponse(response, collectionName, LogCrudType.Replace, entity);
                if (options.Cache.HasFlag(CacheOption.Write))
                {
                    await _cacheHelper.SetAsync(collectionName, response.Resource, options);
                }

                return response.Resource;
            }
            catch (Exception ex)
            {
                LogException(ex, collectionName, entity, nameof(ReplaceAsync));
                throw;
            }
        }

        public async IAsyncEnumerable<T> ToAsyncEnumerable<T>(IQueryable<T> query)
        {
            var iterator = query.ToFeedIterator();
            while (iterator.HasMoreResults)
            {
                var response = await ReadNextAsync(iterator, query);
                foreach (var item in response)
                {
                    yield return item;
                }
            }
        }

        public async Task<IContinuedList<T>> ToContinuedListAsync<T>(IQueryable<T> query)
        {
            var ret = new ContinuedList<T>()
            {
                Documents = new List<T>()
            };

            var iterator = query.ToFeedIterator();
            if (iterator.HasMoreResults)
            {
                var feedRespose = await ReadNextAsync(iterator, query);
                if (!string.IsNullOrEmpty(feedRespose.ContinuationToken))
                {
                    var byteToken = System.Text.Encoding.UTF8.GetBytes(feedRespose.ContinuationToken);
                    ret.ContinuesToken = Convert.ToBase64String(byteToken);
                    ret = await CacheLongCTokenAsync(ret);
                }

                ret.Documents = feedRespose.ToList();
            }

            return ret;
        }

        public async Task<Dictionary<TKey, TValue>> ToDictionaryAsync<TEntity, TKey, TValue>(IQueryable<TEntity> query,
            Func<TEntity, TKey> keySelector,
            Func<TEntity, TValue> valueSelector)
        {
            var collection = new Dictionary<TKey, TValue>();
            var iterator = query.ToFeedIterator();
            while (iterator.HasMoreResults)
            {
                var response = await ReadNextAsync(iterator, query);
                collection = response.ToDictionary(keySelector, valueSelector);
            }

            return collection;
        }

        public async Task<List<T>> ToListAsync<T>(IQueryable<T> query)
        {
            var list = new List<T>();
            var iterator = query.ToFeedIterator();
            while (iterator.HasMoreResults)
            {
                var response = await ReadNextAsync(iterator, query);
                list.AddRange(response);
            }

            return list;
        }

        public async Task<T> UpdateAsync<T>(string collectionName, T entity, Func<T, T> updateAction, IItemOptions options)
            where T : IDocumentEntity
        {
            return await UpdateInternalAsync(collectionName, entity, updateAction, options, 0, 3);
        }

        public async Task<T> UpsertAsync<T>(string collectionName, T entity, IItemOptions options) where T : IDocumentEntity
        {
            var container = GetContainer(collectionName);
            var requestOptions = CreateOptions(options);
            var pk = CreateNullablePartitionKey(options.PartitionKey);
            try
            {
                var response = await container.UpsertItemAsync(entity, pk, requestOptions);
                LogResponse(response, collectionName, LogCrudType.Upsert, entity);
                if (options.Cache.HasFlag(CacheOption.Write))
                {
                    await _cacheHelper.SetAsync(collectionName, response.Resource, options);
                }

                return response.Resource;
            }
            catch (Exception ex)
            {
                LogException(ex, collectionName, entity, nameof(UpsertAsync));
                throw;
            }
        }

        public async Task<T> UpsertAsync<T>(string collectionName, T entity, Func<T, T> updateAction, IItemOptions options)
            where T : IDocumentEntity
        {
            return await UpsertInternalAsync(collectionName, entity, updateAction, options, 0, 3);
        }

        private async Task<List<BulkResponse<T>>> BulkOperationAsync<T>(
            string collectionName,
            IEnumerable<T> entities,
            IItemOptions options,
            BulkMethod<T> operationFunc,
            Func<T, object> getPkFunc,
            LogCrudType logType) where T : IDocumentEntity
        {
            var ret = new List<BulkResponse<T>>();
            var requestOptions = CreateOptions(options);
            var pk = CreatePartitionKey(options.PartitionKey);
            List<Task> tasks = new List<Task>();
            foreach (var entity in entities)
            {
                options.PartitionKey = getPkFunc == null ? options.PartitionKey : getPkFunc(entity);
                pk = getPkFunc != null ? CreatePartitionKey(options.PartitionKey) : pk;
                if (logType != LogCrudType.BulkInsert && !String.IsNullOrEmpty(options.IfMatchEtag))
                {
                    requestOptions = CreateMustMatchOptions(entity.ETag ?? options.IfMatchEtag, requestOptions);
                }

                tasks.Add(operationFunc(entity, pk, requestOptions)
                    .ContinueWith(async (Task<ItemResponse<T>> task) =>
                    {
                        var response = await task;
                        LogResponse(response, collectionName, logType, entity);
                        if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                        {
                            if (options.Cache.HasFlag(CacheOption.Write))
                            {
                                await _cacheHelper.SetAsync(collectionName, response.Resource, options);
                            }

                            ret.Add(new BulkResponse<T>
                            {
                                Document = entity,
                                Status = response.StatusCode
                            });
                            return;
                        }

                        ret.Add(new BulkResponse<T>
                        {
                            Document = entity,
                            Status = response.StatusCode,
                            Message = response.Diagnostics.ToString()
                        });
                    }));
            }

            // Wait until all are done
            await Task.WhenAll(tasks);
            return ret;
        }

        private async Task<ContinuedList<T>> CacheLongCTokenAsync<T>(ContinuedList<T> ret)
        {
            if (ret.ContinuesToken.Length > _options.MaxContinuationTokenLengthBeforeCache)
            {
                var cTokenKey = Guid.NewGuid();
                var isCached = await _cacheHelper.SetCTokenAsync(cTokenKey, ret.ContinuesToken, new ItemOptions()
                {
                    AbsoluteExpiration = DateTimeOffset.UtcNow.AddHours(1)
                });

                if (!isCached && !_options.UseContinuationTokenCollection)
                {
                    return ret;
                }

                if (_options.UseContinuationTokenCollection)
                {
                    var cToken = new ContinuationToken()
                    {
                        Id = cTokenKey.ToString(),
                        Token = ret.ContinuesToken,
                        TimeToLive = 86400, //1 day
                    };

                    await CreateAsync(_options.ContinuationTokenCollectionName,
                        cToken,
                        new ItemOptions() { PartitionKey = cToken.PartitionKey });
                }

                ret.ContinuesToken = cTokenKey.ToString();
            }

            return ret;
        }

        private ItemRequestOptions CreateMustMatchOptions(string eTag, ItemRequestOptions options = null)
        {
            options ??= new ItemRequestOptions();
            if (!string.IsNullOrEmpty(eTag))
            {
                options.IfMatchEtag = eTag;
            }

            return options;
        }

        private ItemRequestOptions CreateNotMatchOptions(string eTag, ItemRequestOptions options = null)
        {
            options ??= new ItemRequestOptions();
            if (!string.IsNullOrEmpty(eTag))
            {
                options.IfNoneMatchEtag = eTag;
            }

            return options;
        }

        private PartitionKey? CreateNullablePartitionKey(object value)
        {
            if (!(value is PartitionKeyCross))
            {
                return CreatePartitionKey(value);
            }

            return null;
        }

        private QueryRequestOptions CreateOptions(IQueryOptions options)
        {
            if (options is null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            QueryRequestOptions queryOptions = new QueryRequestOptions();
            if (!(options.PartitionKey is PartitionKeyCross))
            {
                queryOptions.PartitionKey = CreatePartitionKey(options.PartitionKey);
            }

            queryOptions.MaxItemCount ??= options.MaxItemCount;
            queryOptions.EnableScanInQuery ??= options.EnableScanInQuery;
            queryOptions.SessionToken ??= options.SessionToken;
            queryOptions.IfNoneMatchEtag ??= options.IfNoneMatchEtag;
            queryOptions.IfMatchEtag ??= options.IfMatchEtag;
            queryOptions.ResponseContinuationTokenLimitInKb ??= options.ResponseContinuationTokenLimitInKb;
            return queryOptions;
        }

        private StoredProcedureRequestOptions CreateOptions(IStoredProcOptions options)
        {
            if (options is null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            var queryOptions = new StoredProcedureRequestOptions();
            queryOptions.SessionToken ??= options.SessionToken;
            queryOptions.IfNoneMatchEtag ??= options.IfNoneMatchEtag;
            queryOptions.IfMatchEtag ??= options.IfMatchEtag;
            queryOptions.EnableScriptLogging = options.EnableScriptLogging;
            return queryOptions;
        }

        private ItemRequestOptions CreateOptions(IItemOptions options)
        {
            if (options is null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            ItemRequestOptions queryOptions = new ItemRequestOptions();
            queryOptions.EnableContentResponseOnWrite ??= options.EnableContentResponseOnWrite;
            queryOptions.SessionToken ??= options.SessionToken;
            queryOptions.IfNoneMatchEtag ??= options.IfNoneMatchEtag;
            queryOptions.IfMatchEtag ??= options.IfMatchEtag;
            queryOptions.PostTriggers ??= options.PostTriggers;
            queryOptions.PreTriggers ??= options.PreTriggers;
            return queryOptions;
        }

        private PartitionKey CreatePartitionKey(object value)
        {
            if (value == null)
            {
                return PartitionKey.Null;
            }

            if (value is PartitionKeyNone)
            {
                return PartitionKey.None;
            }

            return (Type.GetTypeCode(value.GetType())) switch
            {
                TypeCode.Int32 => new PartitionKey((int)value),
                TypeCode.String => new PartitionKey((string)value),
                TypeCode.Double => new PartitionKey((double)value),
                TypeCode.Boolean => new PartitionKey((bool)value),
                _ => PartitionKey.None,
            };
        }

        private async IAsyncEnumerable<TransactionResponse> ExecuteTransactionInternalAsync(ITransaction<TransactionalBatch> underlyingTransaction)
        {
            var response = await underlyingTransaction.Context.ExecuteAsync();
            LogResponse(response, $"Transaction: {response.ActivityId}");
            if (response.IsSuccessStatusCode)
            {
                var results = response.Select(x => new TransactionResponse()
                {
                    Document = x.ResourceStream,
                    Status = x.StatusCode
                });

                foreach (var result in results)
                {
                    yield return result;
                }

                yield break;
            }

            yield return new TransactionResponse() { Message = response.ErrorMessage, Status = response.StatusCode };
        }

        private Container GetContainer(string collectionName)
        {
            return _containers.GetOrAdd(collectionName, _db.GetContainer(collectionName));
        }
        private async Task<IQueryOptions> GetCTokenFromCollectionAsync(Guid cTokenKey, IQueryOptions options)
        {
            var cToken = await ReadAsync<ContinuationToken>(_options.ContinuationTokenCollectionName,
                cTokenKey.ToString(), new ItemOptions() { PartitionKey = nameof(ContinuationToken) });
            if (cToken != null)
            {
                options.RequestContinuation = cToken.Token;
            }

            return options;
        }

        private bool IsConcurrencyConflict(CosmosException ex)
        {
            if (ex.StatusCode == HttpStatusCode.PreconditionFailed)
            {
                return true;
            }

            return false;
        }

        private bool IsNotFound(CosmosException ex)
        {
            if (ex.StatusCode == HttpStatusCode.NotFound)
            {
                return true;
            }

            return false;
        }

        private bool IsNotModified(CosmosException ex)
        {
            if (ex.StatusCode == HttpStatusCode.NotModified)
            {
                return true;
            }

            return false;
        }

        private void LogException<T>(Exception ex, string collectionName, T entity, string message)
            where T : IDocumentEntity
        {
            LogException(ex, collectionName, entity.Id, message);
        }

        private void LogException(Exception ex, string collectionName, string id, string message)
        {
            if (ex is CosmosException cosmosEx)
            {
                LogExceptions(cosmosEx, message,
                    (nameof(collectionName), collectionName),
                    (nameof(id), id));

                return;
            }

            LogExceptions(ex, message,
                (nameof(collectionName), collectionName),
                (nameof(id), id));
        }

        private void LogExceptions(CosmosException ex, string message, params (string key, object value)[] properties)
        {
            if (_logger == null || !_options.LogTypes.HasFlag(LogType.Error))
            {
                return;
            }

            var logProps = new Dictionary<string, object>
            {
                { "RequestCharge", ex.RequestCharge },
                { "StatusCode", ex.StatusCode },
                { "SubStatusCode", ex.SubStatusCode },
                { "ErrorMessage", ex.Message },
            };

            if (_options.LogTypes.HasFlag(LogType.Diagnostics))
            {
                logProps["Diagnostics"] = ex.Diagnostics;
            }

            SetCustomLogProps(ref logProps, properties);
            using (_logger.BeginScope(logProps))
            {
                message = $"{nameof(CosmosDbContext)}: {message}";
                _logger.LogError(message);
            }
        }

        private void LogExceptions(Exception ex, string message, params (string key, object value)[] properties)
        {
            if (_logger == null || !_options.LogTypes.HasFlag(LogType.Error))
            {
                return;
            }

            var logProps = new Dictionary<string, object>
            {
                { "HResult", ex.HResult },
                { "ErrorMessage", ex.Message },
            };

            SetCustomLogProps(ref logProps, properties);
            using (_logger.BeginScope(logProps))
            {
                message = $"{nameof(CosmosDbContext)}: {message}";
                _logger.LogError(message);
            }
        }

        private void LogResponse<T>(ItemResponse<T> response, string collectionName, LogCrudType type, T entity, params (string key, object value)[] properties) where T : IDocumentEntity
        {
            string id = response?.Resource?.Id ?? entity.Id;
            LogResponse(response, $"CRUD/{collectionName}/{id}/{type}", properties);
        }

        private void LogResponse<T>(ItemResponse<T> response, string collectionName, string id, LogCrudType type, params (string key, object value)[] properties) where T : IDocumentEntity
        {
            LogResponse(response, $"CRUD/{collectionName}/{id}/{type}", properties);
        }

        private void LogResponse<T>(Response<T> response, string message, params (string key, object value)[] properties)
        {
            if (_logger == null || !_options.LogTypes.HasFlag(LogType.Information))
            {
                return;
            }

            var logProps = new Dictionary<string, object>
            {
                { "RequestCharge", response.RequestCharge },
                { "StatusCode", response.StatusCode }
            };

            if (_options.LogTypes.HasFlag(LogType.Diagnostics))
            {
                logProps["Diagnostics"] = response.Diagnostics;
            }

            SetCustomLogProps(ref logProps, properties);
            using (_logger.BeginScope(logProps

            ))
            {
                message = $"{nameof(CosmosDbContext)}: {message}";
                LogByRequestCharge(message, response.RequestCharge);
            }
        }

        private void LogResponse(TransactionalBatchResponse response, string message, params (string key, object value)[] properties)
        {
            if (_logger == null || !_options.LogTypes.HasFlag(LogType.Information))
            {
                return;
            }

            var logProps = new Dictionary<string, object>
            {
                { "RequestCharge", response.RequestCharge },
                { "StatusCode", response.StatusCode },
                { "ErrorMessage", response.ErrorMessage }
            };

            if (_options.LogTypes.HasFlag(LogType.Diagnostics))
            {
                logProps["Diagnostics"] = response.Diagnostics;
            }

            SetCustomLogProps(ref logProps, properties);
            using (_logger.BeginScope(logProps

            ))
            {
                message = $"{nameof(CosmosDbContext)}: {message}";
                LogByRequestCharge(message, response.RequestCharge);
            }
        }

        private async Task<FeedResponse<T>> ReadNextAsync<T>(FeedIterator<T> iterator, IQueryable<T> query)
        {
            try
            {
                var response = await iterator.ReadNextAsync();
                LogResponse(response, query.ToString());
                return response;
            }
            catch (Exception ex)
            {
                LogExceptions(ex, nameof(ReadNextAsync), ("Query", query.ToString()));
                throw;
            }
        }

        private async Task<FeedResponse<T>> ReadNextAsync<T>(FeedIterator<T> iterator, QueryDefinition def)
        {
            try
            {
                var response = await iterator.ReadNextAsync();
                LogResponse(response, def.QueryText);
                return response;
            }
            catch (Exception ex)
            {
                LogExceptions(ex, nameof(ReadNextAsync), ("Query", def.QueryText));
                throw;
            }
        }

        private void SetCTokenFromCache(ref IQueryOptions options)
        {
            if (options.RequestContinuation.Length == 36 && Guid.TryParse(options.RequestContinuation, out Guid cTokenKey))
            {
                options.RequestContinuation = _cacheHelper.GetCToken(cTokenKey);
                if (!string.IsNullOrEmpty(options.RequestContinuation))
                {
                    _cacheHelper.DeleteCToken(cTokenKey);
                    return;
                }

                if (_options.UseContinuationTokenCollection)
                {
                    IQueryOptions retOptions = null;
                    using (var A = AsyncHelper.Wait)
                    {
                        A.Run(GetCTokenFromCollectionAsync(cTokenKey, options), x => retOptions = x);
                    }
                    options = retOptions;
                }
            }
        }
        private async Task<IQueryOptions> SetCTokenFromCacheAsync(IQueryOptions options)
        {
            if (options.RequestContinuation.Length == 36 && Guid.TryParse(options.RequestContinuation, out Guid cTokenKey))
            {
                options.RequestContinuation = await _cacheHelper.GetCTokenAsync(cTokenKey);
                if (!string.IsNullOrEmpty(options.RequestContinuation))
                {
                    await _cacheHelper.DeleteCTokenAsync(cTokenKey);
                    return options;
                }

                if (_options.UseContinuationTokenCollection)
                {
                    return await GetCTokenFromCollectionAsync(cTokenKey, options);
                }
            }

            return options;
        }
        private void ThrowIfNoCache(IItemOptions itemOptions, string methodName)
        {
            if (itemOptions.Cache != CacheOption.Both)
            {
                throw new NotSupportedException($"{methodName} must always be cached");
            }
        }
        private async Task<T> UpdateInternalAsync<T>(string collectionName, T entity, Func<T, T> updateAction, IItemOptions options, int tries, int maxTries)
            where T : IDocumentEntity
        {
            CheckTries(tries, maxTries);

            var container = GetContainer(collectionName);
            var requestOptions = CreateOptions(options);
            try
            {
                entity = updateAction(entity);
                var response = await container.ReplaceItemAsync(entity, entity.Id,
                    CreatePartitionKey(options.PartitionKey), //cannot be nullable because of ReadAsync
                    CreateMustMatchOptions(entity.ETag, requestOptions));
                LogResponse(response, collectionName, LogCrudType.Update, entity);
                if (options.Cache.HasFlag(CacheOption.Write))
                {
                    await _cacheHelper.SetAsync(collectionName, response.Resource, options);
                }

                return response.Resource;
            }
            catch (CosmosException ex)
            {
                if (IsConcurrencyConflict(ex))
                {
                    entity = await ReadAsync<T>(collectionName, entity.Id, options);
                    return await UpdateInternalAsync(collectionName, entity, updateAction, options, ++tries, maxTries);
                }

                LogException(ex, collectionName, entity, nameof(UpdateInternalAsync));
                throw;
            }
            catch (Exception ex) when (!(ex is CosmosException))
            {
                LogException(ex, collectionName, entity, nameof(UpdateInternalAsync));
                throw;
            }
        }

        private async Task<T> UpsertInternalAsync<T>(string collectionName, T entity, Func<T, T> updateAction, IItemOptions options, int tries, int maxTries)
                    where T : IDocumentEntity
        {
            CheckTries(tries, maxTries);
            var container = GetContainer(collectionName);
            var requestOptions = CreateOptions(options);
            try
            {
                entity = updateAction(entity);
                var response = await container.UpsertItemAsync(entity,
                    CreatePartitionKey(options.PartitionKey), //cannot be nullable because of ReadAsync
                    CreateMustMatchOptions(entity.ETag, requestOptions));
                LogResponse(response, collectionName, LogCrudType.Upsert, entity);
                if (options.Cache.HasFlag(CacheOption.Write))
                {
                    await _cacheHelper.SetAsync(collectionName, response.Resource, options);
                }

                return response.Resource;
            }
            catch (CosmosException ex)
            {
                if (IsConcurrencyConflict(ex))
                {
                    entity = await ReadAsync<T>(collectionName, entity.Id, options);
                    return await UpsertInternalAsync(collectionName, entity, updateAction, options, ++tries, maxTries);
                }

                LogException(ex, collectionName, entity, nameof(UpsertInternalAsync));
                throw;
            }
            catch (Exception ex) when (!(ex is CosmosException))
            {
                LogException(ex, collectionName, entity, nameof(UpsertInternalAsync));
                throw;
            }
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposingClient && disposing && _client != null)
                {
                    _containers.Clear();
                    _client.Dispose();
                    _client = null;
                }

                disposedValue = true;
            }
        }
        #endregion
    }
}
