﻿using Microsoft.Azure.Cosmos;
using System;

namespace Gomez.Core.DocumentDb.Cosmos
{
    public class CosmosOptions : CosmosClientOptions, ICosmosOptions
    {
        public CosmosOptions() : base()
        {

        }

        /// <summary>
        /// Use DefaultCosmosOptions to get options using Iconfiguration and pass to this constructor
        /// </summary>
        /// <param name="options"></param>
        public CosmosOptions(ICosmosOptions options) : this()
        {
            AbsoluteExpirationRelativeToNow = options.AbsoluteExpirationRelativeToNow;
            SlidingExpiration = options.SlidingExpiration;

            ContinuationTokenCollectionName = options.ContinuationTokenCollectionName;
            UseContinuationTokenCollection = options.UseContinuationTokenCollection;
            MaxContinuationTokenLengthBeforeCache = options.MaxContinuationTokenLengthBeforeCache;
        }

        public TimeSpan? AbsoluteExpirationRelativeToNow { get; set; }
        public TimeSpan? SlidingExpiration { get; set; }

        public string ContinuationTokenCollectionName { get; set; }
        public bool UseContinuationTokenCollection { get; set; }
        public int MaxContinuationTokenLengthBeforeCache { get; set; } = 255;
        public LogType LogTypes { get; set; } = LogType.All;
    }
}
