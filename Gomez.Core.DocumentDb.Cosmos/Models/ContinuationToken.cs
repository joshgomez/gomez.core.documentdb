﻿using Newtonsoft.Json;

namespace Gomez.Core.DocumentDb.Cosmos.Models
{
    internal class ContinuationToken : DocumentEntity, IPartitionedDocumentEntity, ITtlDocumentEntity
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("partitionKey")]
        public string PartitionKey { get; set; } = nameof(ContinuationToken);

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "ttl")]
        public int? TimeToLive { get; set; }
    }
}