# Gomez.Core.DocumentDb
Usage please see Gomez.Core.DocumentDb.UnitTest
See the commit details for changelog.

## 2020-09-19
#### New Features
* New Option added too CosmosOptions
	- LogTypes - default: All
* New option added to ItemOptions
	- Cache - default: Both
#### Breaking Changes
* ExecuteSqlAsync
	- Now returns tuple with continuation token